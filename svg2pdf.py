#!/usr/bin/python

# (c) 2015 Vasco Tenner

from __future__ import print_function, division
import tempfile
import subprocess
import time
import shutil
import time
import os
import collections
import xml.etree.ElementTree as ET
import argparse
import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex


def main(svgfile, output=None, debug=False):
    if not output:
        output = os.path.splitext(svgfile)[0]+'.pdf'

    tree = ET.parse(svgfile)
    root = tree.getroot()
    path = ".//svg:g[@inkscape:groupmode='layer']"

    layers = tree.findall(path, namespaces=inkex.NSS)
    layer_ids = [l.get('id') for l in layers]

    reduced_svgs = collections.OrderedDict()
    for layer in layers:
        svg = root.copy()
        label = layer.get(inkex.addNS('label',ns='inkscape'))
        for l in set(layers) - set([layer]):
            svg.remove(l)
        reduced_svgs.update({label: svg})

    def export_to_pdf(layername, svg, outfile_prefix='', debug=False):
        with tempfile.NamedTemporaryFile(suffix='.svg', delete=False) as f:
            ET.ElementTree(reduced_svgs[layername]).write(f)
        print(f.name)
        try:
            execstr = ['inkscape', '--export-pdf', '{}{}.pdf'.format(
                        outfile_prefix, layername),
                        '--without-gui', f.name]
            p = subprocess.Popen(execstr, stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT)
            for line in p.stdout.readlines():
                print(line)

            retval = p.wait()
        except OSError as e:
            print("Execution failed:", e)
        if not debug:
            os.remove(f.name)

    tempdir = tempfile.mkdtemp()
    print('Start time', time.strftime('%H:%M:%S'))
    for n, (layername, svg) in enumerate(reduced_svgs.iteritems()):
        print('Exporting {}/{}: {}'.format(n, len(reduced_svgs), layername))
        export_to_pdf(layername, svg,
                      outfile_prefix=tempdir+'/', debug=debug)
    print('End time', time.strftime('%H:%M:%S'))

    execstr = 'pdftk {}/*.pdf cat output "{}"'.format(tempdir, output)
    p = subprocess.Popen(execstr, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in p.stdout.readlines():
        print(line)

    retval = p.wait()

    if not debug:
        shutil.rmtree(tempdir)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description='Convert a layered svg to a multipage pdf')
    parser.add_argument('svgfile', type=str,
                    help='input svg file')
    parser.add_argument('--output', type=str,
                        help='Output filename, default to input.pdf')
    parser.add_argument('--debug', type=bool,
                        help='Keep source files to make debugging easier')

    args = parser.parse_args()

    main(**vars(args))
